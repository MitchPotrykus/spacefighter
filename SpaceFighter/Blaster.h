
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		// TODO: *Lower cooldown on Blaster*
		m_cooldownSeconds = 0.1;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}


	virtual bool CanFire() const { return m_cooldown <= 0; }

	// EX: Pure Virtual
	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		// TODO: Method explanation

		// Check if weapon is active and can fire
		if (IsActive() && CanFire())
		{
			// Check if weapon contains a trigger type
			if (triggerType.Contains(GetTriggerType()))
			{
				// Load projectile
				Projectile *pProjectile = GetProjectile();

				// If projectile loaded
				if (pProjectile)
				{
					// Activate projectile
					pProjectile->Activate(GetPosition(), true);
					// Set cooldown time
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};